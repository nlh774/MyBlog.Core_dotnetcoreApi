﻿using Blog.Core.Common.DB;
using Blog.Core.IRepository;
using Blog.Core.Model;
using Blog.Core.Repository.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Blog.Core.Repository
{
    public class AdvertisementRepository : BaseRepository<Advertisement>, IAdvertisementRepository
    {

    }
}
