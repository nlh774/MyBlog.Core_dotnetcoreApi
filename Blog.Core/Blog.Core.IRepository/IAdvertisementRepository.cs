﻿using Blog.Core.IRepository.BASE;
using Blog.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Blog.Core.IRepository
{
    public interface IAdvertisementRepository : IBaseRepository<Advertisement>
    {

    }
}
